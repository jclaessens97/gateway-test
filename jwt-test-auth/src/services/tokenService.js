const jwt = require('jsonwebtoken');
const fs = require('fs');

const privateKey = fs.readFileSync('./key/private.pem');

module.exports = {
    createToken: function(payload) {
        Object.assign(payload, {
          exp: 7200000, // 2 hours in ms
        });
        return jwt.sign(payload, privateKey, { algorithm: 'RS256' });
      }
};
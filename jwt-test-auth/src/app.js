const express = require('express');
const bodyParser = require('body-parser');
const tokenService = require('./services/tokenService');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/auth/login', (req, res) => {
    console.log('/auth/login called');

    if (req.body.email === 'test@example.com' && req.body.password == '123') {
        const token = tokenService.createToken({email: req.body.email});
        res.status(200).json(token);
    } else {
        res.sendStatus(400);
    }
});

app.listen(port, () => console.log(`Auth service is listening on port ${port}`));
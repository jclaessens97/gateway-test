const express = require('express');

const app = express();
const port = 4000;

app.get('/api/test', (req, res) => {
    console.log('/api/test called');
    res.status(200).json({message: 'SUCCESSFUL API REQUEST'});
});

app.listen(port, () => console.log(`Backend service is listening on port ${port}`));